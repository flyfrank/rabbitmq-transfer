package rabbitmqtransfer

import (
	"fmt"
	"log"
	"strconv"


	"github.com/streadway/amqp"
)

type MessageType string

const (
	ResponseType MessageType = "response"
	RequestType  MessageType = "request"
	NotifyType   MessageType = "notify"
)


type Messager struct {
	ServiceName   string
	Conn          *amqp.Connection
	Channel       *amqp.Channel
	Queue         amqp.Queue
	ExchangeName  string
	KeyName       string
	EventBus      *MessageEventBus
}

func NewMessager(serviceName, exchangeName, queueName, keyName string, eventBus *MessageEventBus) (*Messager, error) {
	m := &Messager{
		ServiceName:  serviceName,
		ExchangeName: exchangeName,
		KeyName:      keyName,
		EventBus:     eventBus,
	}
	mqUrl := GetUrl()
	var err error
	m.Conn, err = amqp.Dial(mqUrl)
	if err != nil {
		return nil, fmt.Errorf("Failed to connect to RabbitMQ: %w", err)
	}

	m.Channel, err = m.Conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("Failed to open a channel: %w", err)
	}

	err = m.Channel.ExchangeDeclare(
		exchangeName,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to declare an exchange: %w", err)
	}

	m.Queue, err = m.Channel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to declare a queue: %w", err)
	}

	err = m.Channel.QueueBind(
		m.Queue.Name,
		keyName,
		exchangeName,
		false,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to bind a queue: %w", err)
	}

	return m, nil
}

func (m *Messager) Listener() {
	msgs, err := m.Channel.Consume(
		m.Queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %s", err)
		return
	}

	for d := range msgs {
		go func(d amqp.Delivery) {
			reqId, _ := strconv.ParseInt(d.MessageId, 10, 64)
			m.EventBus.OnMessage(reqId, d.Type, d.CorrelationId, d.Body)
		}(d)
	}
}

func (m *Messager) publishMessage(messageType, requestId, routing, message string) error {
	err := m.Channel.Publish(
		m.ExchangeName,
		routing,
		false,
		false,
		amqp.Publishing{
			CorrelationId: m.KeyName,
			Type:          messageType,
			MessageId:     requestId,
			ContentType:   "text/plain",
			Body:          []byte(message),
		})
	if err != nil {
		return fmt.Errorf("Failed to publish a message: %w", err)
	}
	//fmt.Printf("%s send %s: %s\n", m.ServiceName, messageType, message)
	return nil
}

func (m *Messager) Response(requestId, routing, message string) error {
	return m.publishMessage(string(ResponseType), requestId, routing, message)
}

func (m *Messager) Request(requestId, routing, message string) error {
	return m.publishMessage(string(RequestType), requestId, routing, message)
}

func (m *Messager) Notify(requestId, routing, message string) error {
	return m.publishMessage(string(NotifyType), requestId, routing, message)
}
