package rabbitmqtransfer

import "time"

type MessageProxy struct {
	Name string
	Handler         *MessageEventHandler
	OnHandleRequest func(*MessageProxy, *MessageResponse)
	OnHandleNotify  func(*MessageProxy, *MessageResponse)
}

func NewMessageProxy(serviceName, exchangeName, queueName, keyName string) *MessageProxy {
	messageProxy := &MessageProxy{Name : serviceName}
	handler := GetHandler(serviceName, exchangeName, queueName, keyName, func(res *MessageResponse) {
		if messageProxy.OnHandleRequest == nil {
			return
		}
		messageProxy.OnHandleRequest(messageProxy,res)
	}, func(res *MessageResponse) {
		if messageProxy.OnHandleNotify == nil {
			return
		}
		messageProxy.OnHandleNotify(messageProxy,res)
	})
	messageProxy.Handler = handler
	return messageProxy
}

func (m *MessageProxy) MakeRequest(routing, msg string) (chan *MessageResponse, error) {
	result := make(chan *MessageResponse)
	reqId := m.getRequestId()
	m.Handler.SendRequest(reqId, routing, msg)
	m.Handler.On(reqId, func(res *MessageResponse) {
		result <- res
		close(result) // 关闭通道
	})
	return result, nil
}

func (m *MessageProxy) MakeResponse(reqId int64, routing, msg string) {
	m.Handler.SetResponse(reqId, routing, msg)
}

func (m *MessageProxy) MakeNotify(routing, msg string) {
	reqId := m.getRequestId()
	m.Handler.SendNotify(reqId, routing, msg)
}



func (m *MessageProxy) getRequestId() int64 {
	time.Sleep(1 * time.Microsecond)
	return time.Now().UnixMicro()
}



