package rabbitmqtransfer

import (
	"strconv"
)


type MessageEventBus struct {
	Messager       *Messager
	DelegateMessage func(int64,string,string,[]byte)
}

type MessageResponse struct {
	Status     int
	RequestId  int64
	Message    []byte
	IsReply    bool
	IsNotify   bool
	IsRequest  bool
	RoutingKey string
}

func (m *MessageEventBus) SetDelegateMessage(delegate func(int64,string,string,[]byte)) {
	m.DelegateMessage = delegate
}

func (m *MessageEventBus) OnMessage(reqId int64,messageType string, messageFrom string, message []byte) {
	m.DelegateMessage(reqId,messageType,messageFrom,message)
}

func (m *MessageEventBus) Send(reqId int64, routing string, message string) {
	requestId := strconv.FormatInt(reqId, 10)
	m.Messager.Request(requestId, routing, message)
}

func GetMessageEventBus(serviceName string, exchangeName string, queueName string, keyName string) *MessageEventBus {
	return createEventBus(serviceName, exchangeName, queueName, keyName)
}

func createEventBus(serviceName string, exchangeName string, queueName string, keyName string) *MessageEventBus {
	messageEventBus := &MessageEventBus{}
	messageEventBus.Messager,_ = NewMessager(serviceName, exchangeName, queueName, keyName,messageEventBus)
	go messageEventBus.Messager.Listener()
	return messageEventBus
}
