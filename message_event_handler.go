package rabbitmqtransfer

import (
	"strconv"
	"sync"
	"time"
)
type EventBinder struct {
	RequestId       int64
	ContinueHandler func(*MessageResponse)
	CreateTime      int64
}
type MessageEventHandler struct {
	Name            string
	EventBus        *MessageEventBus
	EventBinderList map[int64]*EventBinder
	HandleRequest   func(*MessageResponse)
	Notify          func(*MessageResponse)
	mu              sync.Mutex // 用于保护 EventBinderList 的互斥锁
}

// GetHandler 创建并初始化一个 MessageEventHandler 实例
func GetHandler(serviceName, exchangeName, queueName, keyName string, handleRequest, notify func(*MessageResponse)) *MessageEventHandler {
	eventBus := GetMessageEventBus(serviceName, exchangeName, queueName, keyName)

	handler := &MessageEventHandler{
		Name:            serviceName,
		EventBus:        eventBus,
		EventBinderList: make(map[int64]*EventBinder),
		HandleRequest:   handleRequest,
		Notify:          notify,
	}

	eventBus.SetDelegateMessage(func(reqId int64, messageType, messageFrom string, message []byte) {
		response := &MessageResponse{
			Status:     0,
			RequestId:  reqId,
			Message:    message,
			IsReply:    messageType == string(ResponseType),
			IsNotify:   messageType == string(NotifyType),
			IsRequest:  messageType == string(RequestType),
			RoutingKey: messageFrom,
		}
		handler.Emit(reqId, messageType, response)
	})

	return handler
}

// On 注册一个事件处理函数，用于处理指定请求 ID 的响应
func (m *MessageEventHandler) On(reqId int64, action func(*MessageResponse)) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.EventBinderList[reqId] = &EventBinder{
		RequestId:       reqId,
		ContinueHandler: action,
		CreateTime:      time.Now().UnixMicro(),
	}
}

// Off 注销指定请求 ID 的事件处理函数
func (m *MessageEventHandler) Off(reqId int64) {
	m.mu.Lock()
	defer m.mu.Unlock()

	delete(m.EventBinderList, reqId)
}

// SetResponse 发送响应消息
func (m *MessageEventHandler) SetResponse(reqId int64, routing, message string) {
	requestId := strconv.FormatInt(reqId, 10)
	m.EventBus.Messager.Response(requestId, routing, message)
}

// SendRequest 发送请求消息
func (m *MessageEventHandler) SendRequest(reqId int64, routing, message string) {
	requestId := strconv.FormatInt(reqId, 10)
	m.EventBus.Messager.Request(requestId, routing, message)
}

// SendNotify 发送通知消息
func (m *MessageEventHandler) SendNotify(reqId int64, routing, message string) {
	requestId := strconv.FormatInt(reqId, 10)
	m.EventBus.Messager.Notify(requestId, routing, message)
}

// Emit 触发事件处理函数
func (m *MessageEventHandler) Emit(reqId int64, messageType string, res *MessageResponse) {
	m.mu.Lock()
	defer m.mu.Unlock()

	switch messageType {
	case string(ResponseType):
		if binder, ok := m.EventBinderList[reqId]; ok {
			binder.ContinueHandler(res)
			delete(m.EventBinderList, reqId)
		}
	case string(NotifyType):
		if m.Notify != nil {
			m.Notify(res)
		}
	case string(RequestType):
		if m.HandleRequest != nil {
			m.HandleRequest(res)
		}
	}
}
